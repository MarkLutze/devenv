FROM consol/ubuntu-xfce-vnc AS builder-image

USER 0


# Add some basics
RUN apt-get update && apt-get install -y git-core
RUN apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget

# Grab Pycharm - just doing it early so that do not need to redownload for every build. So making
# it part of the builder image.
WORKDIR /headless
RUN wget https://download.jetbrains.com/python/pycharm-community-2019.1.1.tar.gz && \
    tar -xvzf pycharm-community-2019.1.1.tar.gz && \
    rm pycharm-community-2019.1.1.tar.gz

RUN wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tar.xz
RUN tar -xf Python-3.7.3.tar.xz
WORKDIR Python-3.7.3
RUN ./configure --enable-optimizations
RUN make -j 4
RUN make altinstall
#RUN pip3.7 install virtualenv
RUN python3.7 -m venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"
# Do any needed pip installs here

RUN pip3.7 install --upgrade pip && \
    pip3.7 install --upgrade oauth2client && \
    pip3.7 install --upgrade google-api-python-client && \
    pip3.7 install --upgrade google-cloud-datastore && \
    pip3.7 install --upgrade google-cloud-storage && \
    pip3.7 install --upgrade google-cloud-logging && \
    pip3.7 install --upgrade google-cloud-bigquery && \
    pip3.7 install --upgrade google-cloud-pubsub && \
    pip3.7 install --upgrade google-cloud-vision && \
    pip3.7 install --upgrade pyyaml && \
    pip3.7 install --upgrade behave && \
    pip3.7 install --upgrade messytables && \
    pip3.7 install --upgrade numpy && \
    pip3.7 install --upgrade pandas && \
    pip3.7 install --upgrade python-dateutil && \
    pip3.7 install --upgrade tornado && \
    pip3.7 install --upgrade pysftp && \
    pip3.7 install --upgrade ujson && \
    pip3.7 install --upgrade psycopg2-binary && \
    pip3.7 install --upgrade pymysql && \
    pip3.7 install --upgrade pillow

COPY requirements.txt requirements.txt
RUN pip3.7 install -r requirements.txt

FROM consol/ubuntu-xfce-vnc as dev-image

USER 0

COPY --from=builder-image /usr/local/bin/python3.7 /usr/local/bin/python3.7
COPY --from=builder-image /usr/local/lib/python3.7 /usr/local/lib/python3.7
COPY --from=builder-image /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
RUN chmod +x /opt/venv/bin/activate
RUN apt-get update && apt-get install -y git-core curl

RUN export CLOUD_SDK_REPO="cloud-sdk-xenial" && \
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y

COPY --from=builder-image /headless/pycharm-community-2019.1.1 /headless/pycharm
RUN chown -r 1000:1000 /headless/pycharm
RUN mkdir /headless/dev
RUN chown 1000:1000 /headless/dev

# Switch to root user to install additional software
USER 1000

WORKDIR /headless


